from django.test import TestCase, RequestFactory, Client
from django.urls import reverse
from django.contrib.auth import get_user_model
from tugulenga.views import *
from tugulenga.forms import *



class loginPage_view_test(TestCase):
    def setUp(self):
        #creating a  test user object
        self.username='testusername'
        self.password='testpassword'
        self.user= User.objects.create_user(username=self.username, password=self.password)
        self.client= Client()# creating a client object

    def test_login_successful(self):
        # simulating a POST request with valid data for logging in
        response=self.client.post(reverse("tugule:login"),{"username":self.username, "password":self.password})
        self.assertEqual(response.status_code, 302)# a redirect is expected(status code is 302)

    def test_login_with_invalid_credentials(self):
        #simulating a POST request with invalid credentials
        response=self.client.post(reverse("tugule:login"),{"username":'someusername', "password":'somepassword'})
        expected_message='Username or Password is incorrect'
        self.assertEqual(response.status_code,200)#expecting a successful response (status code is 200)
        self.assertContains(response, expected_message,html=True)

    def test_login_page_render(self):
        #simulating a GET request to render the login page
        response= self.client.get(reverse("tugule:login"))
        self.assertEqual(response.status_code,200)# should be a successful response with status code 200
        self.assertTemplateUsed(response,'tugulenga/login.html')