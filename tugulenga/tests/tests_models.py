from django.test import TestCase
from django.contrib.auth.models import User
from tugulenga.models import *


class Business_accountTest(TestCase):
    def setUp(self):
        #creating a user for one to one field relationship with owner
        self.user= User.objects.create_user(username="testuser", password="somepassword")

        #creating a business account
        self.business=Business_account.objects.create(
            owner= self.user,
            name="Some Business ",
            phone='123456789',
            email='somebusiness@test.com',
            location= 'some place',
            description = 'we gat you',
            logo ='images/logo.png',
        )

    def test_business_account_creation(self):
        self.assertEqual(self.business.name, "Some Business ")
        self.assertEqual(self.business.phone, '123456789')
        self.assertEqual(self.business.email, 'somebusiness@test.com')
        self.assertEqual(self.business.location, 'some place')
        self.assertEqual(self.business.description, 'we gat you')
        self.assertEqual(self.business.logo, 'images/logo.png')

        #checking __str__ method
        self.assertEqual(str(self.business),"Some Business " )