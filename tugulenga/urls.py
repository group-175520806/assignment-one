from django.urls import path
from . import views
app_name="tugule"
urlpatterns=[
    path('',views.home,name="home"),
    path('signup/',views.signup,name="signup"),
    path('login/',views.loginPage,name="login"),
    path('logout/',views.logoutUser,name="logout"),
    path('add_product/', views.add_product, name="add_product"),
    path('profile/', views.profileForm, name= "profile"),
    path('business-profile/', views.business_profile, name= "business-profile"),
    path('business_form/', views.create_businesss, name= "create_business"),
    path('edit-business/', views.edit_business, name= "edit_business"),
    path('edit-product/<str:product_id>', views.edit_product, name="edit_product"),
    path('edit_profile/', views.edit_profile, name= "edit_profile"), 
    path('product_details/<str:product_id>', views.product_detail, name="product_details"),
    path('ratings/<int:product_id>', views.post_rating,name="post_rating"),
    path('search/', views.search, name='search'),
]