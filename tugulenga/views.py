from django.shortcuts import render,redirect
from django.urls import reverse
from django.contrib.auth.models import Group
from django.contrib.auth.forms import  UserCreationForm, AuthenticationForm
from django.contrib.auth import authenticate, login ,logout
from django.contrib import messages
from .decorators import unauthenticated_user
from .models import *
from .forms import *
from django.db.models import Q


# Create your views here.
def home(request):
    
    return render(request,'tugulenga/index.html')

# Decorator to ensure that only unauthenticated users can access this view
@unauthenticated_user
def signup(request):
    # Create an instance of the CreateUserForm
    form = CreateUserForm()
    
    # Check if the request method is POST (i.e., the form has been submitted)
    if request.method == 'POST':
        # Create a new instance of the CreateUserForm, populated with the submitted data
        form = CreateUserForm(data=request.POST)
        
        # Check if the form is valid (i.e., all fields have been filled in correctly)
        if form.is_valid():
            # Save the form data to the database
            form.save()
            
            # Get the username from the form data
            user = form.cleaned_data.get('username')
            
            # Display a success message to the user
            messages.success(request, 'Account was created for ' +user)
            
            # Redirect the user to the login page
            return redirect('tugule:login')
    
    # Create a context dictionary to pass to the template
    context = {"form": form}
    
    # Render the signup HTML template, passing the context dictionary
    return render(request, "tugulenga/signup.html", context)
# Decorator to ensure that only unauthenticated users can access this view
@unauthenticated_user
def loginPage(request):
   
    # Check if the request method is POST (i.e., the user has submitted the login form)
    if request.method == 'POST':
        
        # Get the username and password from the form data
        username = request.POST.get('username')
        password = request.POST.get('password')
        
        # Authenticate the user using the provided username and password
        user = authenticate(request, username=username, password=password)
        
        # If the user is authenticated, log them in and redirect them to the appropriate page
        if user is not None:
            login(request, user)
            try:
                # Check if the user is a business account owner
                business = Business_account.objects.get(owner=request.user)
                
                # Display a success message and redirect the user to the business profile page
                messages.success(request, "Logged In")
                return redirect('tugule:business-profile')
            except:
                # If the user is not a business account owner, redirect them to the home page
                messages.success(request, "Logged In")
                return redirect('products:home')
        else:
            # If the authentication fails, display an error message
            messages.info(request, 'Username or Password is incorrect')
        
    # Render the login HTML template
    return render(request, 'tugulenga/login.html')

# Function to log out the user
def logoutUser(request):
    # Log out the user
    logout(request)
    
    # Display a success message
    messages.success(request, "Logged out")
    
    # Redirect the user to the home page
    return redirect('tugule:home')

  
def add_product(request):
    # Get the business account associated with the current user
    business_account = Business_account.objects.get(owner=request.user)
    
    # Check if the request method is POST (i.e., the form has been submitted)
    if request.method == 'POST':
        # Create a new instance of the ProductForm, populated with the submitted data
        form = ProductForm(request.POST, request.FILES)
        
        # Check if the form is valid (i.e., all fields have been filled in correctly)
        if form.is_valid():
            # Extract the image, category, name, price, and other fields from the form data
            image = form.cleaned_data['image']
            category = form.cleaned_data['category']
            name = form.cleaned_data['name']
            price = form.cleaned_data['price']
            
            # Create a new Product instance with the extracted data
            product = Product.objects.create(
                business_account=business_account,
                image=image,
                name=name,
                price=price,
                category=category,
            )
            
            # Save the product to the database
            product.save()
            
            # Redirect the user to the home page
            return redirect('tugule:business-profile')
    
    # If the request method is not POST (i.e., the form has not been submitted), create a new instance of the ProductForm
    else:
        form = ProductForm()
    
    # Render the add_product HTML template, passing the form instance as a variable
    return render(request, 'tugulenga/add_product.html', {'form': form})

def create_businesss(request):
    # Check if the request method is POST (i.e., the form has been submitted)
    if request.method == 'POST':
        # Create a new instance of the Business_accountForm, populated with the submitted data
        form = Business_accountForm(request.POST, request.FILES)
        
        # Check if the form is valid (i.e., all fields have been filled in correctly)
        if form.is_valid():
            # Create a new Business_account instance with the submitted data, but do not save it yet
            business = form.save(commit=False)
            
            # Set the owner of the business account to the current user
            business.owner = request.user
            
            # Save the business account to the database
            business.save()
            
            # Redirect the user to the business profile page
            return redirect('tugule:business-profile')
    
    # If the request method is not POST (i.e., the form has not been submitted), create a new instance of the Business_accountForm
    else:
        form = Business_accountForm()
    
    # Render the business-form HTML template, passing the form instance as a variable
    return render(request, 'tugulenga/business-form.html', {'form': form})

def edit_business(request):
    # Get the business account associated with the current user
    business = Business_account.objects.get(owner=request.user)
    
    # Check if the request method is POST (i.e., the form has been submitted)
    if request.method == 'POST':
        # Create a new instance of the Business_accountForm, populated with the submitted data and the existing business account instance
        form = Business_accountForm(request.POST, request.FILES, instance=business)
        
        # Check if the form is valid (i.e., all fields have been filled in correctly)
        if form.is_valid():
            # Save the updated business account to the database
            form.save()
            
            # Redirect the user to the business profile page
            return redirect('tugule:business-profile')
        
    # If the request method is not POST (i.e., the form has not been submitted), create a new instance of the Business_accountForm with the existing business account instance
    else:
        form = Business_accountForm(instance=business)
    
    # Create a context dictionary to pass to the template
    context = {'form': form}
    
    # Render the edit-business HTML template, passing the form instance as a variable
    return render(request, 'tugulenga/edit-business.html', context)
def edit_product(request, product_id):
    # Get the product instance with the specified ID
    product = Product.objects.get(id=product_id)
    
    # Check if the request method is POST (i.e., the form has been submitted)
    if request.method == 'POST':
        # Create a new instance of the ProductForm, populated with the submitted data and the existing product instance
        form = ProductForm(request.POST, request.FILES, instance=product)
        
        # Check if the form is valid (i.e., all fields have been filled in correctly)
        if form.is_valid():
            # Save the updated product to the database
            form.save()
            
            # Redirect the user to the business profile page
            return redirect('tugule:business-profile')
    
    # If the request method is not POST (i.e., the form has not been submitted), create a new instance of the ProductForm with the existing product instance
    else:
        form = ProductForm(instance=product)
    
    # Create a context dictionary to pass to the template
    context = {'form': form, "product": product}
    
    # Render the edit-product HTML template, passing the form instance and product instance as variables
    return render(request, 'tugulenga/edit-product.html', context)

    
def profileForm(request):
    # Get the profile instance associated with the current user
    profile = Profile.objects.get(user=request.user)
    
    # Create a context dictionary to pass to the template
    context = {"profile": profile}
    
    # Render the profile HTML template, passing the profile instance as a variable
    return render(request, 'tugulenga/profile.html', context)

def business_profile(request):
    # Get the business account instance associated with the current user
    business = Business_account.objects.get(owner=request.user)
    
    # Get all the product instances associated with the business account
    products = Product.objects.filter(business_account=business)
    
    # Get all the order item instances
    orderitems = OrderItem.objects.all()
    
    # Create a context dictionary to pass to the template
    context = {"business": business, "products": products, "orderitems": orderitems}
    
    # Render the business-profile HTML template, passing the business account instance, product instances, and order item instances as variables
    return render(request, 'tugulenga/business-profile.html', context)

def edit_profile(request):
    # Get the profile instance associated with the current user
    profile = Profile.objects.get(user=request.user)
    form = ProfileForm(instance=profile)
    # Check if the request method is POST (i.e., the form has been submitted)
    if request.method == "POST":
        # Create a new instance of the ProfileForm, populated with the submitted data and the existing profile instance
        form = ProfileForm(request.POST, instance=profile)
        
        # Check if the form is valid (i.e., all fields have been filled in correctly)
        if form.is_valid():
            # Save the updated profile to the database
            form.save()
            
            # Redirect the user to the profile page
            return redirect("tugule:profile")
    context={"form":form}
    return render(request, 'tugulenga/edit_profile.html',context)

def product_detail(request, product_id):
    # Get the product instance with the specified ID
    product = Product.objects.get(id=product_id)
    
    # Get all the rating instances associated with the product
    ratings = Rating.objects.filter(product=product)
    
    # Get all the review instances associated with the product
    reviews = Review.objects.filter(product=product)
    
    # Calculate the average rating of the product
    if len(ratings)!= 0:
        average_rating = sum([x.get_value for x in ratings]) / len(ratings)
        average_rating = math.ceil(average_rating)
        rating_list = [i for i in range(average_rating)]
    else:
        average_rating = 0
        rating_list = []
    
    # Create a context dictionary to pass to the template
    context = {"product": product, "ratings": ratings, "rating_list": rating_list, "reviews": reviews}
    
    # Render the product_details HTML template, passing the product instance, rating instances, and review instances as variables
    return render(request, 'tugulenga/product_details.html', context)
def post_rating(request, product_id):
    # Get the product instance with the specified ID
    product = Product.objects.get(id=product_id)
    
    # Get the rating value from the form data
    rating = request.POST.get('rate')
    
    # Create a new Rating instance with the submitted data
    new = Rating.objects.create(
        user=request.user.profile,
        product=product,
        value=rating
    )
    
    # Save the new Rating instance to the database
    new.save()
    
    # Redirect the user to the product details page
    return redirect('tugule:product_details', product_id)

def search(request):
    product = request.GET.get("search")
    products = Product.objects.filter(Q(name__icontains=product))
    context = {'products':products,}
    return render(request,'products/category.html',context)

    