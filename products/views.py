from django.shortcuts import render,redirect
from tugulenga.models import *
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
import json
from django.contrib import messages


#views to create cart items
# Function to display the contents of a user's shopping cart
@login_required(login_url='tugule:login')
def cart(request):
    # Check if the user is authenticated
    if request.user.is_authenticated:
        # Retrieve the current order for the user
        order, created = Order.objects.get_or_create(customer=request.user, complete = False)
        # Retrieve all the items in the order
        items = order.orderitem_set.all()
    else:    
        # If the user is not authenticated, set the items to an empty list
        items=[]
        # Set the order to a dictionary with two keys: get_cart_total and get_cart_items, both set to 0
        order ={'get_cart_items':0, 'get_cart_total':0}
    # Create a dictionary called context that contains the items and order variables
    context = {'items':items, 
               'order':order}
    # Return an HTTP response that renders the cart.html template and passes the context dictionary to the template
    return render(request,"products/cart.html", context)
@login_required(login_url='tugule:login')
def checkout(request):
    if request.user.is_authenticated:
        order, created = Order.objects.get_or_create(customer=request.user, complete = False)
        items = order.orderitem_set.all()
    else:    
        items=[]
        order ={'get_cart_total':0,'get_cart_items':0}
    context = {'items':items, 
               'order':order}
    return render(request,"products/checkout.html", context)

def update_item(request):
    data = json.loads(request.body)
    productId = data['product_id']
    action = data['action']

    customer = request.user
    product = Product.objects.get(id=productId)
    order,created = Order.objects.get_or_create(customer=customer, complete=False)
    order_item,created = OrderItem.objects.get_or_create(order=order, product=product)
    if action == "add":
        order_item.quantity = (order_item.quantity+1)
        messages.success(request,"Added successfully")
    elif action == "remove":
        order_item.quantity = (order_item.quantity-1)
        messages.success(request,"Deducted successfully")
    

    order_item.save()

    if order_item.quantity <= 0:
        order_item.delete()

    return JsonResponse(action + ' complete', safe=False)



@login_required(login_url='tugule:login')
def home(request):
    categories = Category.objects.all()
    

    context={ "categories":categories}

    return render(request,'products/products.html',context)
@login_required(login_url='tugule:login')
def products(request,category_id):
    
    category=Category.objects.get(id=category_id)
    products=Product.objects.filter(category=category)

    context = {'category':category,'products':products,}
    # print(context)
    return render(request,'products/category.html',context)




@login_required(login_url='tugule:login')
def deleteOrder(request,product_id):
    order=OrderItem.objects.get(id=product_id)
    if request.method=='POST':
        order.delete()
        return redirect('products:cart')
    context={"order":order}

    return render(request,'products/delete.html',context)

def checkout_order(request):
    order = Order.objects.get(customer=request.user, complete=False)
    order.complete = True
    order.save()
    messages.success(request,"Order placed successfully")
    return redirect("products:home")


