var updateBtns = document.getElementsByClassName("update-cart")

for (i = 0; i<updateBtns.length;i++ ){
    updateBtns[i].addEventListener('click',function(){
        var productId = this.dataset.product
        var action = this.dataset.action
        console.log('productId',productId,'Action:', action)

        console.log('USER:', user)
        if (user == 'AnonymousUser'){
            console.log('User is not authenticated')
        }
        else{
            updateUserOrder(productId,action)
        }
    })
}

function updateUserOrder(productId, action){
    console.log('User is authenticated, sending data...')
    var url = '/products/update_item/'

    fetch(url,{
        method:'POST',
        headers:{
            'Content-Type':'application/Json',
            'X-CSRFToken':csrftoken,
        },
        body:JSON.stringify({'product_id':productId, 'action':action})
    })
    .then((response) => {
        return response.json();
    })
    .then((data) =>{
       console.log('Data from server:', data)
       location.reload()
       
    })
}

// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}